var assert = require('assert');
var kmlFunc = require('../arquivo').funcToKml();
var fs = require('fs');


describe('Test1', function() {
  describe('JSON -> KML, test 1', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/1.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/1.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test2', function() {
  describe('JSON -> KML, test 2', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/2.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/2.kml', 'utf8');
		  var output = kmlFunc(input);
     assert.equal(expectedOutput, output);
    });
  });
});

describe('Test3', function() {
  describe('JSON -> KML, test 3', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/3.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/3.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test4', function() {
  describe('JSON -> KML, test 4', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/4.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/4.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test5', function() {
  describe('JSON -> KML, test 5', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/5.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/5.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test6', function() {
  describe('JSON -> KML, test 6', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/6.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/6.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test7', function() {
  describe('JSON -> KML, test 7', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/7.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/7.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test8', function() {
  describe('JSON -> KML, test 8', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/8.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/8.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test9', function() {
  describe('JSON -> KML, test 9', function() {
    it('dot instead of comma error', function() {
		var input = fs.readFileSync(__dirname+'/inputs/9.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/9.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});

describe('Test10', function() {
  describe('JSON -> KML, test 10', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/10.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/10.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test11', function() {
  describe('JSON -> KML, test 11', function() {
    it('should be the same kml', function() {
		var input = fs.readFileSync(__dirname+'/inputs/11.json', 'utf8');
		var expectedOutput = fs.readFileSync(__dirname+'/outputs/11.kml', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, output);
    });
  });
});

describe('Test12', function() {
  describe('JSON -> KML, test 12', function() {
    it('missing open brace error', function() {
		var input = fs.readFileSync(__dirname+'/inputs/12.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});

describe('Test13', function() {
  describe('JSON -> KML, test 13', function() {
    it('missing quotes error', function() {
		var input = fs.readFileSync(__dirname+'/inputs/13.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});

describe('Test14', function() {
  describe('JSON -> KML, test 14', function() {
    it('type not found error', function() {
		var input = fs.readFileSync(__dirname+'/inputs/14.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});

describe('Test15', function() {
  describe('JSON -> KML, test 15', function() {
    it('exceeded value for lon', function() {
		var input = fs.readFileSync(__dirname+'/inputs/15.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});

describe('Test16', function() {
  describe('JSON -> KML, test 16', function() {
    it('exceeded value for lat', function() {
		var input = fs.readFileSync(__dirname+'/inputs/16.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});

describe('Test17', function() {
  describe('JSON -> KML, test 17', function() {
    it('exceeded value for at', function() {
		var input = fs.readFileSync(__dirname+'/inputs/17.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});
describe('Test18', function() {
  describe('JSON -> KML, test 18', function() {
    it('expected a value for lat', function() {
		var input = fs.readFileSync(__dirname+'/inputs/18.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});
describe('Test19', function() {
  describe('JSON -> KML, test 19', function() {
    it('expected a value for lon', function() {
		var input = fs.readFileSync(__dirname+'/inputs/19.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});
describe('Test20', function() {
  describe('JSON -> KML, test 20', function() {
    it('expected a value for at', function() {
		var input = fs.readFileSync(__dirname+'/inputs/20.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});
describe('Test21', function() {
  describe('JSON -> KML, test 21', function() {
    it('no lat tag inside placemark', function() {
		var input = fs.readFileSync(__dirname+'/inputs/21.json', 'utf8');
		var output = kmlFunc(input);
		assert.equal(expectedOutput, -1);
    });
  });
});
