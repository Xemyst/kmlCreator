var antlr4 = require('antlr4');
var MyGrammarLexer = require('../grammar/jsonLexer').jsonLexer;
var MyGrammarParser = require('../grammar/jsonParser').jsonParser;
var MyGrammarListener = require('../grammar/jsonListenerSemantic').jsonListener;
var JsonToKml = require('../grammar/jsonListenerKML').jsonListener;

var input = `{
"kml": [
  {
    "Category": "Folder",
    "id": 1,
    "tags":[
      {
        "type": "placemark",
        "loc":[
          {
          "lat": 10,
          "lon": 10,
          "at": 10,
          "id": 2
        },
        {
        "lat": 1,
        "lon": 1,
        "at": 1,
        "id": 3
        },
        {
        "lat": 11,
        "lon": 11,
        "at": 11,
        "id": 4
        }
        ,
        {
        "lat": 12,
        "lon": 12,
        "at": 12,
        "id": 5
        }
        ,
        {
        "lat": 14,
        "lon": 14,
        "at": 14,
        "id": 6
        }
        ]
      }
    ]
  }
]
}`
var chars = new antlr4.InputStream(input);
var lexer = new MyGrammarLexer(chars);
var tokens  = new antlr4.CommonTokenStream(lexer);
var parser = new MyGrammarParser(tokens);
parser.buildParseTrees = true;
var tree = parser.programa();
var semantic = new MyGrammarListener();
antlr4.tree.ParseTreeWalker.DEFAULT.walk(semantic, tree);
var kml = new JsonToKml();
antlr4.tree.ParseTreeWalker.DEFAULT.walk(kml, tree);
