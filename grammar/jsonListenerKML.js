// Generated from json.g4 by ANTLR 4.7.2
// jshint ignore: start
var antlr4 = require('antlr4/index');
var fs = require('fs')

var data = []


// This class defines a complete listener for a parse tree produced by jsonParser.
function jsonListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

function readTemplate(fileName){
	var text = fs.readFileSync(`./template/${fileName}.txt`,'utf-8')
	return text
}

jsonListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
jsonListener.prototype.constructor = jsonListener;

// Enter a parse tree produced by jsonParser#programa.
jsonListener.prototype.enterPrograma = function(ctx) {
};

// Exit a parse tree produced by jsonParser#programa.
jsonListener.prototype.exitPrograma = function(ctx) {
};


// Enter a parse tree produced by jsonParser#kml.
jsonListener.prototype.enterKml = function(ctx) {
};

// Exit a parse tree produced by jsonParser#kml.
jsonListener.prototype.exitKml = function(ctx) {
};


// Enter a parse tree produced by jsonParser#container.
jsonListener.prototype.enterContainer = function(ctx) {
	//add to array a new folder
};

// Exit a parse tree produced by jsonParser#container.
jsonListener.prototype.exitContainer = function(ctx) {
	//add folders to template
};


// Enter a parse tree produced by jsonParser#tags.
jsonListener.prototype.enterTags = function(ctx) {

};

// Exit a parse tree produced by jsonParser#tags.
jsonListener.prototype.exitTags = function(ctx) {
};


// Enter a parse tree produced by jsonParser#label.
jsonListener.prototype.enterLabel = function(ctx) {
};

// Exit a parse tree produced by jsonParser#label.
jsonListener.prototype.exitLabel = function(ctx) {
	console.log(data)
};


// Enter a parse tree produced by jsonParser#placemark.
jsonListener.prototype.enterPlacemark = async function(ctx) {
	tmp = readTemplate('Placemark')

};

// Exit a parse tree produced by jsonParser#placemark.
jsonListener.prototype.exitPlacemark = function(ctx) {

};


// Enter a parse tree produced by jsonParser#values.
jsonListener.prototype.enterValues = function(ctx) {
	data.push(tmp)
	data[data.length-1] = data[data.length-1].replace('##coordinates##',readTemplate('Coordinates'))
	data[data.length-1] = data[data.length-1].replace('##PLACEMARKID##',ctx.NUMBER()[3].getText())
	data[data.length-1] = data[data.length-1].replace('##LATITUDE##',ctx.NUMBER()[0].getText())
	data[data.length-1] = data[data.length-1].replace('##LONGITUDE##',ctx.NUMBER()[1].getText())
	data[data.length-1] = data[data.length-1].replace('##ALTITUDE##',ctx.NUMBER()[2].getText())
};

// Exit a parse tree produced by jsonParser#values.
jsonListener.prototype.exitValues = function(ctx) {
};

exports.jsonListener = jsonListener;
