// Generated from .\json.g4 by ANTLR 4.8
// jshint ignore: start
var antlr4 = require('antlr4/index');
var jsonListener = require('./jsonListenerKML').jsonListener;
var grammarFileName = "json.g4";


var serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964",
    "\u0003\u0016\u0126\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004",
    "\t\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007",
    "\u0004\b\t\b\u0003\u0002\u0003\u0002\u0007\u0002\u0013\n\u0002\f\u0002",
    "\u000e\u0002\u0016\u000b\u0002\u0003\u0002\u0003\u0002\u0007\u0002\u001a",
    "\n\u0002\f\u0002\u000e\u0002\u001d\u000b\u0002\u0003\u0002\u0003\u0002",
    "\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0007\u0003%\n\u0003",
    "\f\u0003\u000e\u0003(\u000b\u0003\u0003\u0003\u0003\u0003\u0007\u0003",
    ",\n\u0003\f\u0003\u000e\u0003/\u000b\u0003\u0003\u0003\u0003\u0003\u0007",
    "\u00033\n\u0003\f\u0003\u000e\u00036\u000b\u0003\u0003\u0003\u0007\u0003",
    "9\n\u0003\f\u0003\u000e\u0003<\u000b\u0003\u0005\u0003>\n\u0003\u0003",
    "\u0003\u0007\u0003A\n\u0003\f\u0003\u000e\u0003D\u000b\u0003\u0003\u0003",
    "\u0003\u0003\u0003\u0004\u0003\u0004\u0007\u0004J\n\u0004\f\u0004\u000e",
    "\u0004M\u000b\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0007\u0004T\n\u0004\f\u0004\u000e\u0004W\u000b\u0004\u0003",
    "\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0007\u0004^",
    "\n\u0004\f\u0004\u000e\u0004a\u000b\u0004\u0003\u0004\u0003\u0004\u0003",
    "\u0004\u0003\u0004\u0007\u0004g\n\u0004\f\u0004\u000e\u0004j\u000b\u0004",
    "\u0003\u0004\u0003\u0004\u0007\u0004n\n\u0004\f\u0004\u000e\u0004q\u000b",
    "\u0004\u0003\u0004\u0003\u0004\u0007\u0004u\n\u0004\f\u0004\u000e\u0004",
    "x\u000b\u0004\u0003\u0004\u0007\u0004{\n\u0004\f\u0004\u000e\u0004~",
    "\u000b\u0004\u0005\u0004\u0080\n\u0004\u0003\u0004\u0007\u0004\u0083",
    "\n\u0004\f\u0004\u000e\u0004\u0086\u000b\u0004\u0003\u0004\u0003\u0004",
    "\u0007\u0004\u008a\n\u0004\f\u0004\u000e\u0004\u008d\u000b\u0004\u0003",
    "\u0004\u0003\u0004\u0007\u0004\u0091\n\u0004\f\u0004\u000e\u0004\u0094",
    "\u000b\u0004\u0003\u0005\u0003\u0005\u0007\u0005\u0098\n\u0005\f\u0005",
    "\u000e\u0005\u009b\u000b\u0005\u0003\u0005\u0003\u0005\u0007\u0005\u009f",
    "\n\u0005\f\u0005\u000e\u0005\u00a2\u000b\u0005\u0003\u0005\u0003\u0005",
    "\u0007\u0005\u00a6\n\u0005\f\u0005\u000e\u0005\u00a9\u000b\u0005\u0003",
    "\u0005\u0007\u0005\u00ac\n\u0005\f\u0005\u000e\u0005\u00af\u000b\u0005",
    "\u0005\u0005\u00b1\n\u0005\u0003\u0005\u0007\u0005\u00b4\n\u0005\f\u0005",
    "\u000e\u0005\u00b7\u000b\u0005\u0003\u0005\u0003\u0005\u0003\u0006\u0006",
    "\u0006\u00bc\n\u0006\r\u0006\u000e\u0006\u00bd\u0003\u0007\u0003\u0007",
    "\u0003\u0007\u0003\u0007\u0003\u0007\u0007\u0007\u00c5\n\u0007\f\u0007",
    "\u000e\u0007\u00c8\u000b\u0007\u0003\u0007\u0003\u0007\u0003\u0007\u0003",
    "\u0007\u0007\u0007\u00ce\n\u0007\f\u0007\u000e\u0007\u00d1\u000b\u0007",
    "\u0003\u0007\u0003\u0007\u0007\u0007\u00d5\n\u0007\f\u0007\u000e\u0007",
    "\u00d8\u000b\u0007\u0003\u0007\u0003\u0007\u0007\u0007\u00dc\n\u0007",
    "\f\u0007\u000e\u0007\u00df\u000b\u0007\u0003\u0007\u0007\u0007\u00e2",
    "\n\u0007\f\u0007\u000e\u0007\u00e5\u000b\u0007\u0005\u0007\u00e7\n\u0007",
    "\u0003\u0007\u0007\u0007\u00ea\n\u0007\f\u0007\u000e\u0007\u00ed\u000b",
    "\u0007\u0003\u0007\u0003\u0007\u0007\u0007\u00f1\n\u0007\f\u0007\u000e",
    "\u0007\u00f4\u000b\u0007\u0003\b\u0003\b\u0007\b\u00f8\n\b\f\b\u000e",
    "\b\u00fb\u000b\b\u0003\b\u0003\b\u0003\b\u0003\b\u0003\b\u0007\b\u0102",
    "\n\b\f\b\u000e\b\u0105\u000b\b\u0003\b\u0003\b\u0003\b\u0003\b\u0003",
    "\b\u0007\b\u010c\n\b\f\b\u000e\b\u010f\u000b\b\u0003\b\u0003\b\u0003",
    "\b\u0003\b\u0003\b\u0007\b\u0116\n\b\f\b\u000e\b\u0119\u000b\b\u0003",
    "\b\u0003\b\u0003\b\u0003\b\u0007\b\u011f\n\b\f\b\u000e\b\u0122\u000b",
    "\b\u0003\b\u0003\b\u0003\b\u0002\u0002\t\u0002\u0004\u0006\b\n\f\u000e",
    "\u0002\u0002\u0002\u0145\u0002\u0010\u0003\u0002\u0002\u0002\u0004 ",
    "\u0003\u0002\u0002\u0002\u0006G\u0003\u0002\u0002\u0002\b\u0095\u0003",
    "\u0002\u0002\u0002\n\u00bb\u0003\u0002\u0002\u0002\f\u00bf\u0003\u0002",
    "\u0002\u0002\u000e\u00f5\u0003\u0002\u0002\u0002\u0010\u0014\u0007\u0004",
    "\u0002\u0002\u0011\u0013\u0007\n\u0002\u0002\u0012\u0011\u0003\u0002",
    "\u0002\u0002\u0013\u0016\u0003\u0002\u0002\u0002\u0014\u0012\u0003\u0002",
    "\u0002\u0002\u0014\u0015\u0003\u0002\u0002\u0002\u0015\u0017\u0003\u0002",
    "\u0002\u0002\u0016\u0014\u0003\u0002\u0002\u0002\u0017\u001b\u0005\u0004",
    "\u0003\u0002\u0018\u001a\u0007\n\u0002\u0002\u0019\u0018\u0003\u0002",
    "\u0002\u0002\u001a\u001d\u0003\u0002\u0002\u0002\u001b\u0019\u0003\u0002",
    "\u0002\u0002\u001b\u001c\u0003\u0002\u0002\u0002\u001c\u001e\u0003\u0002",
    "\u0002\u0002\u001d\u001b\u0003\u0002\u0002\u0002\u001e\u001f\u0007\u0005",
    "\u0002\u0002\u001f\u0003\u0003\u0002\u0002\u0002 !\u0007\u0003\u0002",
    "\u0002!\"\u0007\b\u0002\u0002\"&\u0007\u0006\u0002\u0002#%\u0007\n\u0002",
    "\u0002$#\u0003\u0002\u0002\u0002%(\u0003\u0002\u0002\u0002&$\u0003\u0002",
    "\u0002\u0002&\'\u0003\u0002\u0002\u0002\'=\u0003\u0002\u0002\u0002(",
    "&\u0003\u0002\u0002\u0002):\u0005\u0006\u0004\u0002*,\u0007\n\u0002",
    "\u0002+*\u0003\u0002\u0002\u0002,/\u0003\u0002\u0002\u0002-+\u0003\u0002",
    "\u0002\u0002-.\u0003\u0002\u0002\u0002.0\u0003\u0002\u0002\u0002/-\u0003",
    "\u0002\u0002\u000204\u0007\u000f\u0002\u000213\u0007\n\u0002\u00022",
    "1\u0003\u0002\u0002\u000236\u0003\u0002\u0002\u000242\u0003\u0002\u0002",
    "\u000245\u0003\u0002\u0002\u000257\u0003\u0002\u0002\u000264\u0003\u0002",
    "\u0002\u000279\u0005\u0006\u0004\u00028-\u0003\u0002\u0002\u00029<\u0003",
    "\u0002\u0002\u0002:8\u0003\u0002\u0002\u0002:;\u0003\u0002\u0002\u0002",
    ";>\u0003\u0002\u0002\u0002<:\u0003\u0002\u0002\u0002=)\u0003\u0002\u0002",
    "\u0002=>\u0003\u0002\u0002\u0002>B\u0003\u0002\u0002\u0002?A\u0007\n",
    "\u0002\u0002@?\u0003\u0002\u0002\u0002AD\u0003\u0002\u0002\u0002B@\u0003",
    "\u0002\u0002\u0002BC\u0003\u0002\u0002\u0002CE\u0003\u0002\u0002\u0002",
    "DB\u0003\u0002\u0002\u0002EF\u0007\u0007\u0002\u0002F\u0005\u0003\u0002",
    "\u0002\u0002GK\u0007\u0004\u0002\u0002HJ\u0007\n\u0002\u0002IH\u0003",
    "\u0002\u0002\u0002JM\u0003\u0002\u0002\u0002KI\u0003\u0002\u0002\u0002",
    "KL\u0003\u0002\u0002\u0002LN\u0003\u0002\u0002\u0002MK\u0003\u0002\u0002",
    "\u0002NO\u0007\f\u0002\u0002OP\u0007\b\u0002\u0002PQ\u0007\r\u0002\u0002",
    "QU\u0007\u000f\u0002\u0002RT\u0007\n\u0002\u0002SR\u0003\u0002\u0002",
    "\u0002TW\u0003\u0002\u0002\u0002US\u0003\u0002\u0002\u0002UV\u0003\u0002",
    "\u0002\u0002VX\u0003\u0002\u0002\u0002WU\u0003\u0002\u0002\u0002XY\u0007",
    "\u0011\u0002\u0002YZ\u0007\b\u0002\u0002Z[\u0007\u0016\u0002\u0002[",
    "_\u0007\u000f\u0002\u0002\\^\u0007\n\u0002\u0002]\\\u0003\u0002\u0002",
    "\u0002^a\u0003\u0002\u0002\u0002_]\u0003\u0002\u0002\u0002_`\u0003\u0002",
    "\u0002\u0002`b\u0003\u0002\u0002\u0002a_\u0003\u0002\u0002\u0002bc\u0007",
    "\u000e\u0002\u0002cd\u0007\b\u0002\u0002dh\u0007\u0006\u0002\u0002e",
    "g\u0007\n\u0002\u0002fe\u0003\u0002\u0002\u0002gj\u0003\u0002\u0002",
    "\u0002hf\u0003\u0002\u0002\u0002hi\u0003\u0002\u0002\u0002i\u007f\u0003",
    "\u0002\u0002\u0002jh\u0003\u0002\u0002\u0002k|\u0005\b\u0005\u0002l",
    "n\u0007\n\u0002\u0002ml\u0003\u0002\u0002\u0002nq\u0003\u0002\u0002",
    "\u0002om\u0003\u0002\u0002\u0002op\u0003\u0002\u0002\u0002pr\u0003\u0002",
    "\u0002\u0002qo\u0003\u0002\u0002\u0002rv\u0007\u000f\u0002\u0002su\u0007",
    "\n\u0002\u0002ts\u0003\u0002\u0002\u0002ux\u0003\u0002\u0002\u0002v",
    "t\u0003\u0002\u0002\u0002vw\u0003\u0002\u0002\u0002wy\u0003\u0002\u0002",
    "\u0002xv\u0003\u0002\u0002\u0002y{\u0005\b\u0005\u0002zo\u0003\u0002",
    "\u0002\u0002{~\u0003\u0002\u0002\u0002|z\u0003\u0002\u0002\u0002|}\u0003",
    "\u0002\u0002\u0002}\u0080\u0003\u0002\u0002\u0002~|\u0003\u0002\u0002",
    "\u0002\u007fk\u0003\u0002\u0002\u0002\u007f\u0080\u0003\u0002\u0002",
    "\u0002\u0080\u0084\u0003\u0002\u0002\u0002\u0081\u0083\u0007\n\u0002",
    "\u0002\u0082\u0081\u0003\u0002\u0002\u0002\u0083\u0086\u0003\u0002\u0002",
    "\u0002\u0084\u0082\u0003\u0002\u0002\u0002\u0084\u0085\u0003\u0002\u0002",
    "\u0002\u0085\u0087\u0003\u0002\u0002\u0002\u0086\u0084\u0003\u0002\u0002",
    "\u0002\u0087\u008b\u0007\u0007\u0002\u0002\u0088\u008a\u0007\n\u0002",
    "\u0002\u0089\u0088\u0003\u0002\u0002\u0002\u008a\u008d\u0003\u0002\u0002",
    "\u0002\u008b\u0089\u0003\u0002\u0002\u0002\u008b\u008c\u0003\u0002\u0002",
    "\u0002\u008c\u008e\u0003\u0002\u0002\u0002\u008d\u008b\u0003\u0002\u0002",
    "\u0002\u008e\u0092\u0007\u0005\u0002\u0002\u008f\u0091\u0007\u000f\u0002",
    "\u0002\u0090\u008f\u0003\u0002\u0002\u0002\u0091\u0094\u0003\u0002\u0002",
    "\u0002\u0092\u0090\u0003\u0002\u0002\u0002\u0092\u0093\u0003\u0002\u0002",
    "\u0002\u0093\u0007\u0003\u0002\u0002\u0002\u0094\u0092\u0003\u0002\u0002",
    "\u0002\u0095\u0099\u0007\u0004\u0002\u0002\u0096\u0098\u0007\n\u0002",
    "\u0002\u0097\u0096\u0003\u0002\u0002\u0002\u0098\u009b\u0003\u0002\u0002",
    "\u0002\u0099\u0097\u0003\u0002\u0002\u0002\u0099\u009a\u0003\u0002\u0002",
    "\u0002\u009a\u00b0\u0003\u0002\u0002\u0002\u009b\u0099\u0003\u0002\u0002",
    "\u0002\u009c\u00ad\u0005\n\u0006\u0002\u009d\u009f\u0007\n\u0002\u0002",
    "\u009e\u009d\u0003\u0002\u0002\u0002\u009f\u00a2\u0003\u0002\u0002\u0002",
    "\u00a0\u009e\u0003\u0002\u0002\u0002\u00a0\u00a1\u0003\u0002\u0002\u0002",
    "\u00a1\u00a3\u0003\u0002\u0002\u0002\u00a2\u00a0\u0003\u0002\u0002\u0002",
    "\u00a3\u00a7\u0007\u000f\u0002\u0002\u00a4\u00a6\u0007\n\u0002\u0002",
    "\u00a5\u00a4\u0003\u0002\u0002\u0002\u00a6\u00a9\u0003\u0002\u0002\u0002",
    "\u00a7\u00a5\u0003\u0002\u0002\u0002\u00a7\u00a8\u0003\u0002\u0002\u0002",
    "\u00a8\u00aa\u0003\u0002\u0002\u0002\u00a9\u00a7\u0003\u0002\u0002\u0002",
    "\u00aa\u00ac\u0005\n\u0006\u0002\u00ab\u00a0\u0003\u0002\u0002\u0002",
    "\u00ac\u00af\u0003\u0002\u0002\u0002\u00ad\u00ab\u0003\u0002\u0002\u0002",
    "\u00ad\u00ae\u0003\u0002\u0002\u0002\u00ae\u00b1\u0003\u0002\u0002\u0002",
    "\u00af\u00ad\u0003\u0002\u0002\u0002\u00b0\u009c\u0003\u0002\u0002\u0002",
    "\u00b0\u00b1\u0003\u0002\u0002\u0002\u00b1\u00b5\u0003\u0002\u0002\u0002",
    "\u00b2\u00b4\u0007\n\u0002\u0002\u00b3\u00b2\u0003\u0002\u0002\u0002",
    "\u00b4\u00b7\u0003\u0002\u0002\u0002\u00b5\u00b3\u0003\u0002\u0002\u0002",
    "\u00b5\u00b6\u0003\u0002\u0002\u0002\u00b6\u00b8\u0003\u0002\u0002\u0002",
    "\u00b7\u00b5\u0003\u0002\u0002\u0002\u00b8\u00b9\u0007\u0005\u0002\u0002",
    "\u00b9\t\u0003\u0002\u0002\u0002\u00ba\u00bc\u0005\f\u0007\u0002\u00bb",
    "\u00ba\u0003\u0002\u0002\u0002\u00bc\u00bd\u0003\u0002\u0002\u0002\u00bd",
    "\u00bb\u0003\u0002\u0002\u0002\u00bd\u00be\u0003\u0002\u0002\u0002\u00be",
    "\u000b\u0003\u0002\u0002\u0002\u00bf\u00c0\u0007\t\u0002\u0002\u00c0",
    "\u00c1\u0007\b\u0002\u0002\u00c1\u00c2\u0007\u0010\u0002\u0002\u00c2",
    "\u00c6\u0007\u000f\u0002\u0002\u00c3\u00c5\u0007\n\u0002\u0002\u00c4",
    "\u00c3\u0003\u0002\u0002\u0002\u00c5\u00c8\u0003\u0002\u0002\u0002\u00c6",
    "\u00c4\u0003\u0002\u0002\u0002\u00c6\u00c7\u0003\u0002\u0002\u0002\u00c7",
    "\u00c9\u0003\u0002\u0002\u0002\u00c8\u00c6\u0003\u0002\u0002\u0002\u00c9",
    "\u00ca\u0007\u0012\u0002\u0002\u00ca\u00cb\u0007\b\u0002\u0002\u00cb",
    "\u00cf\u0007\u0006\u0002\u0002\u00cc\u00ce\u0007\n\u0002\u0002\u00cd",
    "\u00cc\u0003\u0002\u0002\u0002\u00ce\u00d1\u0003\u0002\u0002\u0002\u00cf",
    "\u00cd\u0003\u0002\u0002\u0002\u00cf\u00d0\u0003\u0002\u0002\u0002\u00d0",
    "\u00e6\u0003\u0002\u0002\u0002\u00d1\u00cf\u0003\u0002\u0002\u0002\u00d2",
    "\u00e3\u0005\u000e\b\u0002\u00d3\u00d5\u0007\n\u0002\u0002\u00d4\u00d3",
    "\u0003\u0002\u0002\u0002\u00d5\u00d8\u0003\u0002\u0002\u0002\u00d6\u00d4",
    "\u0003\u0002\u0002\u0002\u00d6\u00d7\u0003\u0002\u0002\u0002\u00d7\u00d9",
    "\u0003\u0002\u0002\u0002\u00d8\u00d6\u0003\u0002\u0002\u0002\u00d9\u00dd",
    "\u0007\u000f\u0002\u0002\u00da\u00dc\u0007\n\u0002\u0002\u00db\u00da",
    "\u0003\u0002\u0002\u0002\u00dc\u00df\u0003\u0002\u0002\u0002\u00dd\u00db",
    "\u0003\u0002\u0002\u0002\u00dd\u00de\u0003\u0002\u0002\u0002\u00de\u00e0",
    "\u0003\u0002\u0002\u0002\u00df\u00dd\u0003\u0002\u0002\u0002\u00e0\u00e2",
    "\u0005\u000e\b\u0002\u00e1\u00d6\u0003\u0002\u0002\u0002\u00e2\u00e5",
    "\u0003\u0002\u0002\u0002\u00e3\u00e1\u0003\u0002\u0002\u0002\u00e3\u00e4",
    "\u0003\u0002\u0002\u0002\u00e4\u00e7\u0003\u0002\u0002\u0002\u00e5\u00e3",
    "\u0003\u0002\u0002\u0002\u00e6\u00d2\u0003\u0002\u0002\u0002\u00e6\u00e7",
    "\u0003\u0002\u0002\u0002\u00e7\u00eb\u0003\u0002\u0002\u0002\u00e8\u00ea",
    "\u0007\n\u0002\u0002\u00e9\u00e8\u0003\u0002\u0002\u0002\u00ea\u00ed",
    "\u0003\u0002\u0002\u0002\u00eb\u00e9\u0003\u0002\u0002\u0002\u00eb\u00ec",
    "\u0003\u0002\u0002\u0002\u00ec\u00ee\u0003\u0002\u0002\u0002\u00ed\u00eb",
    "\u0003\u0002\u0002\u0002\u00ee\u00f2\u0007\u0007\u0002\u0002\u00ef\u00f1",
    "\u0007\u000f\u0002\u0002\u00f0\u00ef\u0003\u0002\u0002\u0002\u00f1\u00f4",
    "\u0003\u0002\u0002\u0002\u00f2\u00f0\u0003\u0002\u0002\u0002\u00f2\u00f3",
    "\u0003\u0002\u0002\u0002\u00f3\r\u0003\u0002\u0002\u0002\u00f4\u00f2",
    "\u0003\u0002\u0002\u0002\u00f5\u00f9\u0007\u0004\u0002\u0002\u00f6\u00f8",
    "\u0007\n\u0002\u0002\u00f7\u00f6\u0003\u0002\u0002\u0002\u00f8\u00fb",
    "\u0003\u0002\u0002\u0002\u00f9\u00f7\u0003\u0002\u0002\u0002\u00f9\u00fa",
    "\u0003\u0002\u0002\u0002\u00fa\u00fc\u0003\u0002\u0002\u0002\u00fb\u00f9",
    "\u0003\u0002\u0002\u0002\u00fc\u00fd\u0007\u0013\u0002\u0002\u00fd\u00fe",
    "\u0007\b\u0002\u0002\u00fe\u00ff\u0007\u0016\u0002\u0002\u00ff\u0103",
    "\u0007\u000f\u0002\u0002\u0100\u0102\u0007\n\u0002\u0002\u0101\u0100",
    "\u0003\u0002\u0002\u0002\u0102\u0105\u0003\u0002\u0002\u0002\u0103\u0101",
    "\u0003\u0002\u0002\u0002\u0103\u0104\u0003\u0002\u0002\u0002\u0104\u0106",
    "\u0003\u0002\u0002\u0002\u0105\u0103\u0003\u0002\u0002\u0002\u0106\u0107",
    "\u0007\u0014\u0002\u0002\u0107\u0108\u0007\b\u0002\u0002\u0108\u0109",
    "\u0007\u0016\u0002\u0002\u0109\u010d\u0007\u000f\u0002\u0002\u010a\u010c",
    "\u0007\n\u0002\u0002\u010b\u010a\u0003\u0002\u0002\u0002\u010c\u010f",
    "\u0003\u0002\u0002\u0002\u010d\u010b\u0003\u0002\u0002\u0002\u010d\u010e",
    "\u0003\u0002\u0002\u0002\u010e\u0110\u0003\u0002\u0002\u0002\u010f\u010d",
    "\u0003\u0002\u0002\u0002\u0110\u0111\u0007\u0015\u0002\u0002\u0111\u0112",
    "\u0007\b\u0002\u0002\u0112\u0113\u0007\u0016\u0002\u0002\u0113\u0117",
    "\u0007\u000f\u0002\u0002\u0114\u0116\u0007\n\u0002\u0002\u0115\u0114",
    "\u0003\u0002\u0002\u0002\u0116\u0119\u0003\u0002\u0002\u0002\u0117\u0115",
    "\u0003\u0002\u0002\u0002\u0117\u0118\u0003\u0002\u0002\u0002\u0118\u011a",
    "\u0003\u0002\u0002\u0002\u0119\u0117\u0003\u0002\u0002\u0002\u011a\u011b",
    "\u0007\u0011\u0002\u0002\u011b\u011c\u0007\b\u0002\u0002\u011c\u0120",
    "\u0007\u0016\u0002\u0002\u011d\u011f\u0007\n\u0002\u0002\u011e\u011d",
    "\u0003\u0002\u0002\u0002\u011f\u0122\u0003\u0002\u0002\u0002\u0120\u011e",
    "\u0003\u0002\u0002\u0002\u0120\u0121\u0003\u0002\u0002\u0002\u0121\u0123",
    "\u0003\u0002\u0002\u0002\u0122\u0120\u0003\u0002\u0002\u0002\u0123\u0124",
    "\u0007\u0005\u0002\u0002\u0124\u000f\u0003\u0002\u0002\u0002)\u0014",
    "\u001b&-4:=BKU_hov|\u007f\u0084\u008b\u0092\u0099\u00a0\u00a7\u00ad",
    "\u00b0\u00b5\u00bd\u00c6\u00cf\u00d6\u00dd\u00e3\u00e6\u00eb\u00f2\u00f9",
    "\u0103\u010d\u0117\u0120"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

var sharedContextCache = new antlr4.PredictionContextCache();

var literalNames = [ null, "'\"kml\"'", "'{'", "'}'", "'['", "']'", "':'", 
                     "'\"type\"'", null, null, "'\"Category\"'", "'\"Folder\"'", 
                     "'\"tags\"'", "','", "'\"placemark\"'", "'\"id\"'", 
                     "'\"loc\"'", "'\"lat\"'", "'\"lon\"'", "'\"at\"'" ];

var symbolicNames = [ null, "KML", "OPEN_BRACE", "CLOSE_BRACE", "OPEN_BRACKET", 
                      "CLOSE_BRACKET", "COLON", "TYPE", "LINE_BREAK", "SPACE", 
                      "CATEGORY", "FOLDER", "TAGS", "COMMA", "PLACEMARK", 
                      "ID", "LOCATION", "LATITUDE", "LONGITUDE", "ALTITUDE", 
                      "NUMBER" ];

var ruleNames =  [ "programa", "kml", "container", "tags", "label", "placemark", 
                   "values" ];

function jsonParser (input) {
	antlr4.Parser.call(this, input);
    this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
    this.ruleNames = ruleNames;
    this.literalNames = literalNames;
    this.symbolicNames = symbolicNames;
    return this;
}

jsonParser.prototype = Object.create(antlr4.Parser.prototype);
jsonParser.prototype.constructor = jsonParser;

Object.defineProperty(jsonParser.prototype, "atn", {
	get : function() {
		return atn;
	}
});

jsonParser.EOF = antlr4.Token.EOF;
jsonParser.KML = 1;
jsonParser.OPEN_BRACE = 2;
jsonParser.CLOSE_BRACE = 3;
jsonParser.OPEN_BRACKET = 4;
jsonParser.CLOSE_BRACKET = 5;
jsonParser.COLON = 6;
jsonParser.TYPE = 7;
jsonParser.LINE_BREAK = 8;
jsonParser.SPACE = 9;
jsonParser.CATEGORY = 10;
jsonParser.FOLDER = 11;
jsonParser.TAGS = 12;
jsonParser.COMMA = 13;
jsonParser.PLACEMARK = 14;
jsonParser.ID = 15;
jsonParser.LOCATION = 16;
jsonParser.LATITUDE = 17;
jsonParser.LONGITUDE = 18;
jsonParser.ALTITUDE = 19;
jsonParser.NUMBER = 20;

jsonParser.RULE_programa = 0;
jsonParser.RULE_kml = 1;
jsonParser.RULE_container = 2;
jsonParser.RULE_tags = 3;
jsonParser.RULE_label = 4;
jsonParser.RULE_placemark = 5;
jsonParser.RULE_values = 6;


function ProgramaContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = jsonParser.RULE_programa;
    return this;
}

ProgramaContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ProgramaContext.prototype.constructor = ProgramaContext;

ProgramaContext.prototype.OPEN_BRACE = function() {
    return this.getToken(jsonParser.OPEN_BRACE, 0);
};

ProgramaContext.prototype.kml = function() {
    return this.getTypedRuleContext(KmlContext,0);
};

ProgramaContext.prototype.CLOSE_BRACE = function() {
    return this.getToken(jsonParser.CLOSE_BRACE, 0);
};

ProgramaContext.prototype.LINE_BREAK = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.LINE_BREAK);
    } else {
        return this.getToken(jsonParser.LINE_BREAK, i);
    }
};


ProgramaContext.prototype.enterRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.enterPrograma(this);
	}
};

ProgramaContext.prototype.exitRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.exitPrograma(this);
	}
};




jsonParser.ProgramaContext = ProgramaContext;

jsonParser.prototype.programa = function() {

    var localctx = new ProgramaContext(this, this._ctx, this.state);
    this.enterRule(localctx, 0, jsonParser.RULE_programa);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 14;
        this.match(jsonParser.OPEN_BRACE);
        this.state = 18;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 15;
            this.match(jsonParser.LINE_BREAK);
            this.state = 20;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 21;
        this.kml();
        this.state = 25;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 22;
            this.match(jsonParser.LINE_BREAK);
            this.state = 27;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 28;
        this.match(jsonParser.CLOSE_BRACE);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function KmlContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = jsonParser.RULE_kml;
    return this;
}

KmlContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
KmlContext.prototype.constructor = KmlContext;

KmlContext.prototype.KML = function() {
    return this.getToken(jsonParser.KML, 0);
};

KmlContext.prototype.COLON = function() {
    return this.getToken(jsonParser.COLON, 0);
};

KmlContext.prototype.OPEN_BRACKET = function() {
    return this.getToken(jsonParser.OPEN_BRACKET, 0);
};

KmlContext.prototype.CLOSE_BRACKET = function() {
    return this.getToken(jsonParser.CLOSE_BRACKET, 0);
};

KmlContext.prototype.LINE_BREAK = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.LINE_BREAK);
    } else {
        return this.getToken(jsonParser.LINE_BREAK, i);
    }
};


KmlContext.prototype.container = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ContainerContext);
    } else {
        return this.getTypedRuleContext(ContainerContext,i);
    }
};

KmlContext.prototype.COMMA = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.COMMA);
    } else {
        return this.getToken(jsonParser.COMMA, i);
    }
};


KmlContext.prototype.enterRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.enterKml(this);
	}
};

KmlContext.prototype.exitRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.exitKml(this);
	}
};




jsonParser.KmlContext = KmlContext;

jsonParser.prototype.kml = function() {

    var localctx = new KmlContext(this, this._ctx, this.state);
    this.enterRule(localctx, 2, jsonParser.RULE_kml);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 30;
        this.match(jsonParser.KML);
        this.state = 31;
        this.match(jsonParser.COLON);
        this.state = 32;
        this.match(jsonParser.OPEN_BRACKET);
        this.state = 36;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,2,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 33;
                this.match(jsonParser.LINE_BREAK); 
            }
            this.state = 38;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,2,this._ctx);
        }

        this.state = 59;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===jsonParser.OPEN_BRACE) {
            this.state = 39;
            this.container();
            this.state = 56;
            this._errHandler.sync(this);
            var _alt = this._interp.adaptivePredict(this._input,5,this._ctx)
            while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
                if(_alt===1) {
                    this.state = 43;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    while(_la===jsonParser.LINE_BREAK) {
                        this.state = 40;
                        this.match(jsonParser.LINE_BREAK);
                        this.state = 45;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                    }
                    this.state = 46;
                    this.match(jsonParser.COMMA);
                    this.state = 50;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    while(_la===jsonParser.LINE_BREAK) {
                        this.state = 47;
                        this.match(jsonParser.LINE_BREAK);
                        this.state = 52;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                    }
                    this.state = 53;
                    this.container(); 
                }
                this.state = 58;
                this._errHandler.sync(this);
                _alt = this._interp.adaptivePredict(this._input,5,this._ctx);
            }

        }

        this.state = 64;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 61;
            this.match(jsonParser.LINE_BREAK);
            this.state = 66;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 67;
        this.match(jsonParser.CLOSE_BRACKET);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function ContainerContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = jsonParser.RULE_container;
    return this;
}

ContainerContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ContainerContext.prototype.constructor = ContainerContext;

ContainerContext.prototype.OPEN_BRACE = function() {
    return this.getToken(jsonParser.OPEN_BRACE, 0);
};

ContainerContext.prototype.CATEGORY = function() {
    return this.getToken(jsonParser.CATEGORY, 0);
};

ContainerContext.prototype.COLON = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.COLON);
    } else {
        return this.getToken(jsonParser.COLON, i);
    }
};


ContainerContext.prototype.FOLDER = function() {
    return this.getToken(jsonParser.FOLDER, 0);
};

ContainerContext.prototype.COMMA = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.COMMA);
    } else {
        return this.getToken(jsonParser.COMMA, i);
    }
};


ContainerContext.prototype.ID = function() {
    return this.getToken(jsonParser.ID, 0);
};

ContainerContext.prototype.NUMBER = function() {
    return this.getToken(jsonParser.NUMBER, 0);
};

ContainerContext.prototype.TAGS = function() {
    return this.getToken(jsonParser.TAGS, 0);
};

ContainerContext.prototype.OPEN_BRACKET = function() {
    return this.getToken(jsonParser.OPEN_BRACKET, 0);
};

ContainerContext.prototype.CLOSE_BRACKET = function() {
    return this.getToken(jsonParser.CLOSE_BRACKET, 0);
};

ContainerContext.prototype.CLOSE_BRACE = function() {
    return this.getToken(jsonParser.CLOSE_BRACE, 0);
};

ContainerContext.prototype.LINE_BREAK = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.LINE_BREAK);
    } else {
        return this.getToken(jsonParser.LINE_BREAK, i);
    }
};


ContainerContext.prototype.tags = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(TagsContext);
    } else {
        return this.getTypedRuleContext(TagsContext,i);
    }
};

ContainerContext.prototype.enterRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.enterContainer(this);
	}
};

ContainerContext.prototype.exitRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.exitContainer(this);
	}
};




jsonParser.ContainerContext = ContainerContext;

jsonParser.prototype.container = function() {

    var localctx = new ContainerContext(this, this._ctx, this.state);
    this.enterRule(localctx, 4, jsonParser.RULE_container);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 69;
        this.match(jsonParser.OPEN_BRACE);
        this.state = 73;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 70;
            this.match(jsonParser.LINE_BREAK);
            this.state = 75;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 76;
        this.match(jsonParser.CATEGORY);
        this.state = 77;
        this.match(jsonParser.COLON);
        this.state = 78;
        this.match(jsonParser.FOLDER);
        this.state = 79;
        this.match(jsonParser.COMMA);
        this.state = 83;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 80;
            this.match(jsonParser.LINE_BREAK);
            this.state = 85;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 86;
        this.match(jsonParser.ID);
        this.state = 87;
        this.match(jsonParser.COLON);
        this.state = 88;
        this.match(jsonParser.NUMBER);
        this.state = 89;
        this.match(jsonParser.COMMA);
        this.state = 93;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 90;
            this.match(jsonParser.LINE_BREAK);
            this.state = 95;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 96;
        this.match(jsonParser.TAGS);
        this.state = 97;
        this.match(jsonParser.COLON);
        this.state = 98;
        this.match(jsonParser.OPEN_BRACKET);
        this.state = 102;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,11,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 99;
                this.match(jsonParser.LINE_BREAK); 
            }
            this.state = 104;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,11,this._ctx);
        }

        this.state = 125;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===jsonParser.OPEN_BRACE) {
            this.state = 105;
            this.tags();
            this.state = 122;
            this._errHandler.sync(this);
            var _alt = this._interp.adaptivePredict(this._input,14,this._ctx)
            while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
                if(_alt===1) {
                    this.state = 109;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    while(_la===jsonParser.LINE_BREAK) {
                        this.state = 106;
                        this.match(jsonParser.LINE_BREAK);
                        this.state = 111;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                    }
                    this.state = 112;
                    this.match(jsonParser.COMMA);
                    this.state = 116;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    while(_la===jsonParser.LINE_BREAK) {
                        this.state = 113;
                        this.match(jsonParser.LINE_BREAK);
                        this.state = 118;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                    }
                    this.state = 119;
                    this.tags(); 
                }
                this.state = 124;
                this._errHandler.sync(this);
                _alt = this._interp.adaptivePredict(this._input,14,this._ctx);
            }

        }

        this.state = 130;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 127;
            this.match(jsonParser.LINE_BREAK);
            this.state = 132;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 133;
        this.match(jsonParser.CLOSE_BRACKET);
        this.state = 137;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 134;
            this.match(jsonParser.LINE_BREAK);
            this.state = 139;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 140;
        this.match(jsonParser.CLOSE_BRACE);
        this.state = 144;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,18,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 141;
                this.match(jsonParser.COMMA); 
            }
            this.state = 146;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,18,this._ctx);
        }

    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function TagsContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = jsonParser.RULE_tags;
    return this;
}

TagsContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
TagsContext.prototype.constructor = TagsContext;

TagsContext.prototype.OPEN_BRACE = function() {
    return this.getToken(jsonParser.OPEN_BRACE, 0);
};

TagsContext.prototype.CLOSE_BRACE = function() {
    return this.getToken(jsonParser.CLOSE_BRACE, 0);
};

TagsContext.prototype.LINE_BREAK = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.LINE_BREAK);
    } else {
        return this.getToken(jsonParser.LINE_BREAK, i);
    }
};


TagsContext.prototype.label = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(LabelContext);
    } else {
        return this.getTypedRuleContext(LabelContext,i);
    }
};

TagsContext.prototype.COMMA = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.COMMA);
    } else {
        return this.getToken(jsonParser.COMMA, i);
    }
};


TagsContext.prototype.enterRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.enterTags(this);
	}
};

TagsContext.prototype.exitRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.exitTags(this);
	}
};




jsonParser.TagsContext = TagsContext;

jsonParser.prototype.tags = function() {

    var localctx = new TagsContext(this, this._ctx, this.state);
    this.enterRule(localctx, 6, jsonParser.RULE_tags);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 147;
        this.match(jsonParser.OPEN_BRACE);
        this.state = 151;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,19,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 148;
                this.match(jsonParser.LINE_BREAK); 
            }
            this.state = 153;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,19,this._ctx);
        }

        this.state = 174;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===jsonParser.TYPE) {
            this.state = 154;
            this.label();
            this.state = 171;
            this._errHandler.sync(this);
            var _alt = this._interp.adaptivePredict(this._input,22,this._ctx)
            while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
                if(_alt===1) {
                    this.state = 158;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    while(_la===jsonParser.LINE_BREAK) {
                        this.state = 155;
                        this.match(jsonParser.LINE_BREAK);
                        this.state = 160;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                    }
                    this.state = 161;
                    this.match(jsonParser.COMMA);
                    this.state = 165;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    while(_la===jsonParser.LINE_BREAK) {
                        this.state = 162;
                        this.match(jsonParser.LINE_BREAK);
                        this.state = 167;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                    }
                    this.state = 168;
                    this.label(); 
                }
                this.state = 173;
                this._errHandler.sync(this);
                _alt = this._interp.adaptivePredict(this._input,22,this._ctx);
            }

        }

        this.state = 179;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 176;
            this.match(jsonParser.LINE_BREAK);
            this.state = 181;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 182;
        this.match(jsonParser.CLOSE_BRACE);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function LabelContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = jsonParser.RULE_label;
    return this;
}

LabelContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LabelContext.prototype.constructor = LabelContext;

LabelContext.prototype.placemark = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(PlacemarkContext);
    } else {
        return this.getTypedRuleContext(PlacemarkContext,i);
    }
};

LabelContext.prototype.enterRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.enterLabel(this);
	}
};

LabelContext.prototype.exitRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.exitLabel(this);
	}
};




jsonParser.LabelContext = LabelContext;

jsonParser.prototype.label = function() {

    var localctx = new LabelContext(this, this._ctx, this.state);
    this.enterRule(localctx, 8, jsonParser.RULE_label);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 185; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 184;
            this.placemark();
            this.state = 187; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===jsonParser.TYPE);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function PlacemarkContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = jsonParser.RULE_placemark;
    return this;
}

PlacemarkContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PlacemarkContext.prototype.constructor = PlacemarkContext;

PlacemarkContext.prototype.TYPE = function() {
    return this.getToken(jsonParser.TYPE, 0);
};

PlacemarkContext.prototype.COLON = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.COLON);
    } else {
        return this.getToken(jsonParser.COLON, i);
    }
};


PlacemarkContext.prototype.PLACEMARK = function() {
    return this.getToken(jsonParser.PLACEMARK, 0);
};

PlacemarkContext.prototype.COMMA = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.COMMA);
    } else {
        return this.getToken(jsonParser.COMMA, i);
    }
};


PlacemarkContext.prototype.LOCATION = function() {
    return this.getToken(jsonParser.LOCATION, 0);
};

PlacemarkContext.prototype.OPEN_BRACKET = function() {
    return this.getToken(jsonParser.OPEN_BRACKET, 0);
};

PlacemarkContext.prototype.CLOSE_BRACKET = function() {
    return this.getToken(jsonParser.CLOSE_BRACKET, 0);
};

PlacemarkContext.prototype.LINE_BREAK = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.LINE_BREAK);
    } else {
        return this.getToken(jsonParser.LINE_BREAK, i);
    }
};


PlacemarkContext.prototype.values = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ValuesContext);
    } else {
        return this.getTypedRuleContext(ValuesContext,i);
    }
};

PlacemarkContext.prototype.enterRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.enterPlacemark(this);
	}
};

PlacemarkContext.prototype.exitRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.exitPlacemark(this);
	}
};




jsonParser.PlacemarkContext = PlacemarkContext;

jsonParser.prototype.placemark = function() {

    var localctx = new PlacemarkContext(this, this._ctx, this.state);
    this.enterRule(localctx, 10, jsonParser.RULE_placemark);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 189;
        this.match(jsonParser.TYPE);
        this.state = 190;
        this.match(jsonParser.COLON);
        this.state = 191;
        this.match(jsonParser.PLACEMARK);
        this.state = 192;
        this.match(jsonParser.COMMA);
        this.state = 196;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 193;
            this.match(jsonParser.LINE_BREAK);
            this.state = 198;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 199;
        this.match(jsonParser.LOCATION);
        this.state = 200;
        this.match(jsonParser.COLON);
        this.state = 201;
        this.match(jsonParser.OPEN_BRACKET);
        this.state = 205;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,27,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 202;
                this.match(jsonParser.LINE_BREAK); 
            }
            this.state = 207;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,27,this._ctx);
        }

        this.state = 228;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===jsonParser.OPEN_BRACE) {
            this.state = 208;
            this.values();
            this.state = 225;
            this._errHandler.sync(this);
            var _alt = this._interp.adaptivePredict(this._input,30,this._ctx)
            while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
                if(_alt===1) {
                    this.state = 212;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    while(_la===jsonParser.LINE_BREAK) {
                        this.state = 209;
                        this.match(jsonParser.LINE_BREAK);
                        this.state = 214;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                    }
                    this.state = 215;
                    this.match(jsonParser.COMMA);
                    this.state = 219;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    while(_la===jsonParser.LINE_BREAK) {
                        this.state = 216;
                        this.match(jsonParser.LINE_BREAK);
                        this.state = 221;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                    }
                    this.state = 222;
                    this.values(); 
                }
                this.state = 227;
                this._errHandler.sync(this);
                _alt = this._interp.adaptivePredict(this._input,30,this._ctx);
            }

        }

        this.state = 233;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 230;
            this.match(jsonParser.LINE_BREAK);
            this.state = 235;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 236;
        this.match(jsonParser.CLOSE_BRACKET);
        this.state = 240;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,33,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 237;
                this.match(jsonParser.COMMA); 
            }
            this.state = 242;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,33,this._ctx);
        }

    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function ValuesContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = jsonParser.RULE_values;
    return this;
}

ValuesContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ValuesContext.prototype.constructor = ValuesContext;

ValuesContext.prototype.OPEN_BRACE = function() {
    return this.getToken(jsonParser.OPEN_BRACE, 0);
};

ValuesContext.prototype.LATITUDE = function() {
    return this.getToken(jsonParser.LATITUDE, 0);
};

ValuesContext.prototype.COLON = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.COLON);
    } else {
        return this.getToken(jsonParser.COLON, i);
    }
};


ValuesContext.prototype.NUMBER = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.NUMBER);
    } else {
        return this.getToken(jsonParser.NUMBER, i);
    }
};


ValuesContext.prototype.COMMA = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.COMMA);
    } else {
        return this.getToken(jsonParser.COMMA, i);
    }
};


ValuesContext.prototype.LONGITUDE = function() {
    return this.getToken(jsonParser.LONGITUDE, 0);
};

ValuesContext.prototype.ALTITUDE = function() {
    return this.getToken(jsonParser.ALTITUDE, 0);
};

ValuesContext.prototype.ID = function() {
    return this.getToken(jsonParser.ID, 0);
};

ValuesContext.prototype.CLOSE_BRACE = function() {
    return this.getToken(jsonParser.CLOSE_BRACE, 0);
};

ValuesContext.prototype.LINE_BREAK = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(jsonParser.LINE_BREAK);
    } else {
        return this.getToken(jsonParser.LINE_BREAK, i);
    }
};


ValuesContext.prototype.enterRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.enterValues(this);
	}
};

ValuesContext.prototype.exitRule = function(listener) {
    if(listener instanceof jsonListener ) {
        listener.exitValues(this);
	}
};




jsonParser.ValuesContext = ValuesContext;

jsonParser.prototype.values = function() {

    var localctx = new ValuesContext(this, this._ctx, this.state);
    this.enterRule(localctx, 12, jsonParser.RULE_values);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 243;
        this.match(jsonParser.OPEN_BRACE);
        this.state = 247;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 244;
            this.match(jsonParser.LINE_BREAK);
            this.state = 249;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 250;
        this.match(jsonParser.LATITUDE);
        this.state = 251;
        this.match(jsonParser.COLON);
        this.state = 252;
        this.match(jsonParser.NUMBER);
        this.state = 253;
        this.match(jsonParser.COMMA);
        this.state = 257;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 254;
            this.match(jsonParser.LINE_BREAK);
            this.state = 259;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 260;
        this.match(jsonParser.LONGITUDE);
        this.state = 261;
        this.match(jsonParser.COLON);
        this.state = 262;
        this.match(jsonParser.NUMBER);
        this.state = 263;
        this.match(jsonParser.COMMA);
        this.state = 267;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 264;
            this.match(jsonParser.LINE_BREAK);
            this.state = 269;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 270;
        this.match(jsonParser.ALTITUDE);
        this.state = 271;
        this.match(jsonParser.COLON);
        this.state = 272;
        this.match(jsonParser.NUMBER);
        this.state = 273;
        this.match(jsonParser.COMMA);
        this.state = 277;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 274;
            this.match(jsonParser.LINE_BREAK);
            this.state = 279;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 280;
        this.match(jsonParser.ID);
        this.state = 281;
        this.match(jsonParser.COLON);
        this.state = 282;
        this.match(jsonParser.NUMBER);
        this.state = 286;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===jsonParser.LINE_BREAK) {
            this.state = 283;
            this.match(jsonParser.LINE_BREAK);
            this.state = 288;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 289;
        this.match(jsonParser.CLOSE_BRACE);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


exports.jsonParser = jsonParser;
