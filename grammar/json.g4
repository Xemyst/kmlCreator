grammar json;

programa : OPEN_BRACE LINE_BREAK* kml LINE_BREAK* CLOSE_BRACE;
kml: KML COLON OPEN_BRACKET LINE_BREAK* (container (LINE_BREAK* COMMA LINE_BREAK* container)*)? LINE_BREAK* CLOSE_BRACKET;
container: OPEN_BRACE LINE_BREAK* CATEGORY COLON FOLDER COMMA LINE_BREAK* ID COLON NUMBER COMMA LINE_BREAK* TAGS COLON OPEN_BRACKET LINE_BREAK* (tags (LINE_BREAK* COMMA LINE_BREAK* tags)*)? LINE_BREAK* CLOSE_BRACKET LINE_BREAK* CLOSE_BRACE COMMA*;
tags: OPEN_BRACE LINE_BREAK* (label (LINE_BREAK* COMMA LINE_BREAK* label)*)?  LINE_BREAK* CLOSE_BRACE;
label: placemark+;
placemark: TYPE COLON PLACEMARK COMMA LINE_BREAK* LOCATION COLON OPEN_BRACKET LINE_BREAK* (values (LINE_BREAK* COMMA LINE_BREAK* values)*)? LINE_BREAK* CLOSE_BRACKET COMMA*;
values:  OPEN_BRACE LINE_BREAK* LATITUDE COLON NUMBER COMMA LINE_BREAK* LONGITUDE COLON NUMBER COMMA LINE_BREAK* ALTITUDE COLON NUMBER COMMA LINE_BREAK* ID COLON NUMBER LINE_BREAK* CLOSE_BRACE  ;


KML           : '"kml"';
OPEN_BRACE    : '{';
CLOSE_BRACE   : '}';
OPEN_BRACKET  : '[';
CLOSE_BRACKET : ']';
COLON         : ':';
TYPE          : '"type"';
LINE_BREAK    : ('\r'? '\n' | '\r' )+;
SPACE         : (' ' | '\t')+ -> channel(HIDDEN);
CATEGORY      : '"Category"';
FOLDER        : '"Folder"';
TAGS          : '"tags"';
COMMA         : ',';
PLACEMARK     : '"placemark"';
ID            : '"id"';
LOCATION      : '"loc"';
LATITUDE      : '"lat"';
LONGITUDE     : '"lon"';
ALTITUDE      : '"at"';
NUMBER        : [0-9]+ ([.,] [0-9]+)?;
