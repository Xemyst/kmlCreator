// Generated from json.g4 by ANTLR 4.7.2
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by jsonParser.
function jsonListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

jsonListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
jsonListener.prototype.constructor = jsonListener;

// Enter a parse tree produced by jsonParser#programa.
jsonListener.prototype.enterPrograma = function(ctx) {
};

// Exit a parse tree produced by jsonParser#programa.
jsonListener.prototype.exitPrograma = function(ctx) {
};


// Enter a parse tree produced by jsonParser#kml.
jsonListener.prototype.enterKml = function(ctx) {
};

// Exit a parse tree produced by jsonParser#kml.
jsonListener.prototype.exitKml = function(ctx) {
};


// Enter a parse tree produced by jsonParser#container.
jsonListener.prototype.enterContainer = function(ctx) {
	//add to array a new folder
};

// Exit a parse tree produced by jsonParser#container.
jsonListener.prototype.exitContainer = function(ctx) {
	//add folders to template
};


// Enter a parse tree produced by jsonParser#tags.
jsonListener.prototype.enterTags = function(ctx) {

};

// Exit a parse tree produced by jsonParser#tags.
jsonListener.prototype.exitTags = function(ctx) {
};


// Enter a parse tree produced by jsonParser#label.
jsonListener.prototype.enterLabel = function(ctx) {
};

// Exit a parse tree produced by jsonParser#label.
jsonListener.prototype.exitLabel = function(ctx) {
	//add to folders
};


// Enter a parse tree produced by jsonParser#placemark.
jsonListener.prototype.enterPlacemark = function(ctx) {

};

// Exit a parse tree produced by jsonParser#placemark.
jsonListener.prototype.exitPlacemark = function(ctx) {
	//add to array of labels
};


// Enter a parse tree produced by jsonParser#values.
jsonListener.prototype.enterValues = function(ctx) {
	//Set variables
	let lat = Number(ctx.NUMBER()[0].getText())
	let log = Number(ctx.NUMBER()[1].getText())
	let at  = Number(ctx.NUMBER()[2].getText())
	//console.log(ctx.NUMBER()[0].getText())

	if(-90 > lat || lat > 90)
		console.log("Error: latitude value not allowed ")
	if(-180 > log || log > 180)
		console.log("Error: longitude value not allowed")
	if(0 > at || at > 800000)
		console.log("Error: altitude value not allowed")

};

// Exit a parse tree produced by jsonParser#values.
jsonListener.prototype.exitValues = function(ctx) {
};



exports.jsonListener = jsonListener;
