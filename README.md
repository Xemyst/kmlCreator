# KmlCreator


### Examples
###### Json file
```json
{
"TAG": "xyz",
"KML": [
  {
    "Category": "folder",
    "id"  : 1,
    "tags":[
      {
        "type": "placemark",
        "id": 2,
        "loc":[
          {
          "lat": 10,
          "lon": 10,
          "at": 10
          }
        ]
      }
    ]
  }
]
}
```
###### Kml file
```html
<xml>
<kml>
  <folder id="1">
    <placemark id="2">
      <point>
        <coordinates>
          10,10,10
        </coordinates>
      </point>
    </placemark>
  </folder>
</kml>
```
